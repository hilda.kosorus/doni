
var express = require('express');
var request = require('request');
var app = express();
var bodyParser = require('body-parser')

app.use(bodyParser.json())


var port = process.env.PORT || 8080;
var token = 'EAACWaRoYSRQBAHtaY82R2R8ZAbZCgxzPmdtMBdQxdjiEt1dUpVVLA4WZB5yK2X0I37o6MyhZAwdZCSy1njGZC4T0tBIGdLRcoJO29vCZCJiKWHfYKJUmh5VinbrbnGv1KQT8At8RoSfewWNWtoNT6nEaZCg2NFiZACBiZBmVeO3BJZA3AMYbpmZCkCVN'
app.get('/', function(req, res) {
    res.send('index');
});

app.get('/webhook/', function (req, res) {
  if (req.query['hub.verify_token'] === '1234') {
    res.send(req.query['hub.challenge']);
  }
  res.send('Error, wrong validation token');
})


app.post('/webhook/', function (req, res) {
	console.error('req.body', JSON.stringify(req.body.entry[0].messaging[0].sender))
  messaging_events = req.body.entry[0].messaging;
  for (i = 0; i < messaging_events.length; i++) {
    event = req.body.entry[0].messaging[i];
    sender = event.sender.id;
    if (event.message && event.message.text) {
      text = event.message.text;
         if (text.indexOf('Hello') > -1 || text.indexOf('hello') > -1) {
            sendTextMessage(sender, 'Hello!')
            sendWorkoutMessage(sender)
         }
         else {
            if (text.indexOf('picture') > -1  || text.indexOf('photo') > -1 || text.indexOf('Photo') > -1 || text.indexOf('Picture') > -1) {
                showPicture(sender)
            }
            else {
                sendTextMessage(sender, 'I did not understand that.')
            }
         }
    }
    if (event.postback) {
         text = JSON.stringify(event.postback);
         if (text.indexOf('results') > -1) {
            sendResultsMessage(sender)
         }
         if (text.indexOf('running') > -1) {
            sendRunningMessage(sender)
         }
         if (text.indexOf('now') > -1) {
            sendResultsWorkoutChoiceMessage(sender)
         }
         if (text.indexOf('later') > -1) {
            sendScheduleChoiceMessage(sender)
         }
         if (text.indexOf('6pm') > -1) {
            sendTextMessage(sender, 'Ok, your workout is scheduled for 6 p.m. I set a reminder for you!')
         }
         if (text.indexOf('7pm') > -1) {
            sendTextMessage(sender, 'Ok, your workout is scheduled for 7 p.m. I set a reminder for you!')
         }
         if (text.indexOf('yes') > -1) {
            sendTextMessage(sender, ':*')
         }
         if (text.indexOf('no!') > -1) {
            sendTextMessage(sender, ':(')
         }
         continue;
    }
  }
  res.sendStatus(200);
});



function sendTextMessage(sender, text) {
  messageData = {
    text:text
  }
  request({
    url: 'https://graph.facebook.com/v2.6/me/messages',
    qs: {access_token:token},
    method: 'POST',
    json: {
      recipient: {id:sender},
      message: messageData,
    }
  }, function(error, response, body) {
    if (error) {
      console.log('Error sending message: ', error);
    } else if (response.body.error) {
      console.log('Error: ', response.body.error);
    }
  });
}

function showPicture(sender) {
    messageData = {
        "attachment": {
            "type": "template",
            "payload": {
                "template_type": "generic",
                "elements": [{
                             "title": "Do you like what you see? ;)",
                             "subtitle": "",
                             "image_url": "https://scontent.xx.fbcdn.net/t31.0-8/13071959_679218705550891_4406368563085926554_o.jpg",
                             "buttons": [{
                                         "type": "postback",
                                         "payload": "yes",
                                         "title": "Yes"
                                         }, {
                                         "type": "postback",
                                         "title": "No",
                                         "payload": "no!",
                                         }],
                             }]
            }
        }
    };
    request({
            url: 'https://graph.facebook.com/v2.6/me/messages',
            qs: {access_token:token},
            method: 'POST',
            json: {
            recipient: {id:sender},
            message: messageData,
            }
            }, function(error, response, body) {
            if (error) {
            console.log('Error sending message: ', error);
            } else if (response.body.error) {
            console.log('Error: ', response.body.error);
            }
            });
}

function sendGenericMessage(sender) {
  messageData = {
    "attachment": {
      "type": "template",
      "payload": {
        "template_type": "generic",
        "elements": [{
          "title": "First card",
          "subtitle": "Element #1 of an hscroll",
          "image_url": "http://messengerdemo.parseapp.com/img/rift.png",
          "buttons": [{
            "type": "web_url",
            "url": "https://www.messenger.com/",
            "title": "Web url"
          }, {
            "type": "postback",
            "title": "Postback",
            "payload": "Payload for first element in a generic bubble",
          }],
        },{
          "title": "Second card",
          "subtitle": "Element #2 of an hscroll",
          "image_url": "http://messengerdemo.parseapp.com/img/gearvr.png",
          "buttons": [{
            "type": "postback",
            "title": "Postback",
            "payload": "Payload for second element in a generic bubble",
          }],
        }]
      }
    }
  };
  request({
    url: 'https://graph.facebook.com/v2.6/me/messages',
    qs: {access_token:token},
    method: 'POST',
    json: {
      recipient: {id:sender},
      message: messageData,
    }
  }, function(error, response, body) {
    if (error) {
      console.log('Error sending message: ', error);
    } else if (response.body.error) {
      console.log('Error: ', response.body.error);
    }
  });
}


function sendMorningRunMessage(sender) {
    messageData = {
        "attachment":{
            "type":"template",
            "payload":{
                "template_type":"button",
                "text":"Would you like to go for a morning run?",
                "buttons":[
                           {
                           "type":"postback",
                           "title":"Yes",
                           "payload":"yes-morning-run"
                           },
                           {
                           "type":"postback",
                           "title":"No",
                           "payload":"no-morning-run"
                           }
                           ]
            }
        }
    };
    request({
            url: 'https://graph.facebook.com/v2.6/me/messages',
            qs: {access_token:token},
            method: 'POST',
            json: {
            recipient: {id:sender},
            message: messageData,
            }
            }, function(error, response, body) {
            if (error) {
            console.log('Error sending message: ', error);
            } else if (response.body.error) {
            console.log('Error: ', response.body.error);
            }
            });
}

function sendWorkoutMessage(sender) {
    messageData = {
        "attachment":{
            "type":"template",
            "payload":{
                "template_type":"button",
                "text":"What would you like to do today?",
                "buttons":[
                           {
                           "type":"postback",
                           "title":"Results workout",
                           "payload":"results"
                           },
                           {
                           "type":"postback",
                           "title":"Running",
                           "payload":"running"
                           }
                           ]
            }
        }
    };
    request({
            url: 'https://graph.facebook.com/v2.6/me/messages',
            qs: {access_token:token},
            method: 'POST',
            json: {
            recipient: {id:sender},
            message: messageData,
            }
            }, function(error, response, body) {
            if (error) {
            console.log('Error sending message: ', error);
            } else if (response.body.error) {
            console.log('Error: ', response.body.error);
            }
            });
}


function sendResultsMessage(sender) {
    messageData = {
        "attachment":{
            "type":"template",
            "payload":{
                "template_type":"button",
                "text":"When would you like to start?",
                "buttons":[
                           {
                           "type":"postback",
                           "title":"Now",
                           "payload":"now"
                           },
                           {
                           "type":"postback",
                           "title":"Later",
                           "payload":"later"
                           }
                           ]
            }
        }
    };
    request({
            url: 'https://graph.facebook.com/v2.6/me/messages',
            qs: {access_token:token},
            method: 'POST',
            json: {
            recipient: {id:sender},
            message: messageData,
            }
            }, function(error, response, body) {
            if (error) {
            console.log('Error sending message: ', error);
            } else if (response.body.error) {
            console.log('Error: ', response.body.error);
            }
            });
}

function sendRunningMessage(sender) {
    messageData = {
        "attachment":{
            "type":"template",
            "payload":{
                "template_type":"button",
                "text":"Would you like to start now or later?",
                "buttons":[
                           {
                           "type":"postback",
                           "title":"Now",
                           "payload":"running-now"
                           },
                           {
                           "type":"postback",
                           "title":"Later",
                           "payload":"running-later"
                           }
                           ]
            }
        }
    };
    request({
            url: 'https://graph.facebook.com/v2.6/me/messages',
            qs: {access_token:token},
            method: 'POST',
            json: {
            recipient: {id:sender},
            message: messageData,
            }
            }, function(error, response, body) {
            if (error) {
            console.log('Error sending message: ', error);
            } else if (response.body.error) {
            console.log('Error: ', response.body.error);
            }
            });
}

function sendResultsWorkoutChoiceMessage(sender) {
    messageData = {
        "attachment":{
            "type":"template",
            "payload":{
                "template_type":"button",
                "text":"Which workout do you prefer?",
                "buttons":[
                           {
                           "type":"web_url",
                           "title":"Alpha",
                           "url":"https://www.runtastic.com/workouts/body-transformation/alpha"
                           },
                           {
                           "type":"web_url",
                           "title":"Bravo",
                           "url":"https://www.runtastic.com/workouts/body-transformation/bravo"
                           },
                           {
                           "type":"web_url",
                           "title":"Other",
                           "url":"https://www.runtastic.com/workouts/body-transformation"
                           }
                           ]
            }
        }
    };
    request({
            url: 'https://graph.facebook.com/v2.6/me/messages',
            qs: {access_token:token},
            method: 'POST',
            json: {
            recipient: {id:sender},
            message: messageData,
            }
            }, function(error, response, body) {
            if (error) {
            console.log('Error sending message: ', error);
            } else if (response.body.error) {
            console.log('Error: ', response.body.error);
            }
            });
}

function sendScheduleChoiceMessage(sender) {
    messageData = {
        "attachment":{
            "type":"template",
            "payload":{
                "template_type":"button",
                "text":"At what time should I remind you?",
                "buttons":[
                           {
                           "type":"postback",
                           "title":"6 p.m.",
                           "payload":"6pm"
                           },
                           {
                           "type":"postback",
                           "title":"7 p.m.",
                           "payload":"7pm"
                           }
                           ]
            }
        }
    };
    request({
            url: 'https://graph.facebook.com/v2.6/me/messages',
            qs: {access_token:token},
            method: 'POST',
            json: {
            recipient: {id:sender},
            message: messageData,
            }
            }, function(error, response, body) {
            if (error) {
            console.log('Error sending message: ', error);
            } else if (response.body.error) {
            console.log('Error: ', response.body.error);
            }
            });
}



app.listen(port, function() {
    console.log('Our app is running on http://localhost:' + port);
});
